const sidebar = document.querySelector("#description-article-sidebar")

const showSidebar = () => {
    if (document.documentElement.scrollTop > 1000) {
        sidebar.style.display = "flex"
    } else {
        sidebar.style.display = "none"
    }
}

document.addEventListener("scroll", showSidebar);


let list = document.querySelectorAll(".list")
let main = document.querySelector(".main")
for (let i = 0; i < list.length; i++) {
    list[i].addEventListener("click", () => {
        main.style.top = (i * -100) + "%"
        // 点击第一个的时候i是0，又因为在if判断里面0为false,所以当i为1或2的时候触发背景渐变
        // 再在判断里加一层三目表达式，分别判断i等等于1和i等等于2时的背景色
        
    })
}